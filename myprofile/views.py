from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Schedule
response = {}


def index(request):
     return render(request, 'myprofile/index.html',{})

def posts(request):
     return render(request, 'myprofile/posts.html',{})

def message_table(request):
    message = Schedule.objects.all()
    response['output'] = message
    html = 'myprofile/posts.html'
    return render(request, html , response)

def delete_all(request):
    message = Schedule.objects.all().delete()
    response['output'] = message
    html = 'myprofile/posts.html'
    return render(request, html , response)

def add(request):
    if  request.method == 'POST':
        print("MASUK SINI")
        response['name']=request.POST['name']
        response['day']=request.POST['day']
        response['date']=request.POST['date']
        response['time']=request.POST['time']
        response['location']=request.POST.get('location')
        response['category']=request.POST.get('category')
        message= Schedule(name=response['name'], day =response['day'], date=response['date'], time=response['time'], location=response['location'], category=response['category'])
        message.save()
        print(Schedule.objects.all().values())
        response['output'] = message.__class__.objects.all()
        return render(request, 'myprofile/posts.html', response)
    else:
        return HttpResponseRedirect('myprofile/posts.html')
