from django.urls import path

from . import views

urlpatterns = [

    path('', views.index, name='index'),
	path('story5-add', views.add, name='story5-add'),
    path('posts/', views.message_table, name='posts'),
    path('story5-del', views.delete_all, name='story5-del'),
]
