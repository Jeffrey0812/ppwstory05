from django.db import models
from django.forms import ModelForm

DAY_CHOICES = (
	("MONDAY", "Monday"),
	("TUESDAY", "Tuesday"),
	("WEDNESDAY", "Wednesday"),
	("THURSDAY", "Thursday"),
	("FRIDAY", "Friday"),
	("SATURDAY", "Saturday"),
	("SUNDAY", "Sunday"),
)

class Schedule(models.Model):
	name = models.CharField(max_length=100)
	day = models.CharField(max_length=8, choices=DAY_CHOICES)
	date = models.DateField(blank=True, null=True)
	time = models.TimeField()
	location = models.CharField(max_length=100)
	category = models.CharField(max_length=100)
